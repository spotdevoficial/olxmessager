package br.com.spotdev.olxmessager;

import java.io.IOException;
import java.text.SimpleDateFormat;

import br.com.spotdev.olxmessager.scrapper.Utils;
import br.com.spotdev.olxmessager.scrapper.account.Account;
import br.com.spotdev.olxmessager.scrapper.connection.Connection;
import br.com.spotdev.olxmessager.scrapper.connection.Envio;
import br.com.spotdev.olxmessager.scrapper.enums.Categoria;
import br.com.spotdev.olxmessager.scrapper.enums.ErrorCode;
import br.com.spotdev.olxmessager.scrapper.enums.Estado;
import br.com.spotdev.olxmessager.scrapper.enums.Tipo;
import br.com.spotdev.olxmessager.scrapper.events.OLXEnvioEvent;
import br.com.spotdev.olxmessager.scrapper.excpetions.SendingAlreadyStarted;

/**
 * @Descrição Classe principal da aplicacão
 * 
 * @Autor Davi Salles
 * @Data 14/04/2016
 */

public class OLXMessager {
	
	// Valores est�ticos para a conex�o
	public static Connection CONNECTION;		// Conexão do usuário
	public static String SEED = null;			// Seed para geração
	public static String PASSWORD = null;		// Senha para login
	public static int MESSAGE_LIMIT = 10;		// Limite de mensagens por conta
	
	
	public static void main(String args[]) throws IOException{
		
	    String message = Utils.formatUntraceable("+55 47 99749-8920 das das das das dasdasd dasd@hotmail.com dasd@hotmail.com hotmail.com dasd@hotmail.com");
	    System.out.println(message);
	    
		Connection CONNECTION = new Connection("gabriel", "bigarismo");
		CONNECTION.setEstado(Estado.BRASIL);
		CONNECTION.setCategoria(Categoria.TODAS);
		CONNECTION.setTipo(Tipo.TODOS);
		CONNECTION.setSendAllMessagesToOne(true);

		try {
			while(!CONNECTION.registerNew());
			CONNECTION.addMessage("Oláaa! Cara que anúncio top! se liga o meu http://www.google.com.br/");
		} catch (IOException e1) {
			e1.printStackTrace();
		}
			
		try {
			
			CONNECTION.startEnvio(new OLXEnvioEvent() {
				int i = 0;
				@Override
				public void envioRealizado(Envio envio) {

					Account account = envio.getAccount();
					
					String email = account.getEmail();
					// Mostra mensagem de sucesso se der certo e erro se der errado
					if(envio.isSucesso()){						// SUCESSO
						i++;

						SimpleDateFormat dt = new SimpleDateFormat("hh:mm:ss"); 
						System.out.println("["+dt.format(envio.getDate())+"]["+email+"]["+CONNECTION.getActualPage()+"]["+CONNECTION.getEnviados()+"] Envio realizado com sucesso: "+envio.getAnuncio().getUrl());
					}else{										// ERRO
						SimpleDateFormat dt = new SimpleDateFormat("hh:mm:ss"); 
						System.out.println("["+dt.format(envio.getDate())+"]["+email+"]["+CONNECTION.getActualPage()+"]["+CONNECTION.getEnviados()+"] "+envio.getMensagem()+": "+envio.getAnuncio().getUrl());
					}
					
					if(i == 5 || envio.getErrorCode() == ErrorCode.UNAUTHORIZED){
						i = 0;
						account.stop();
					}
				}
				@Override
				public void envioIniciado(Connection connection) {
					System.out.println("Envio iniciado");
				}
				@Override
				public void envioFinalizado(Connection connection) {
					// TODO Auto-generated method stub
					
				}
			}, 0);
			
		} catch (SendingAlreadyStarted e) {
			e.printStackTrace();
		}
		
	}
	
}
