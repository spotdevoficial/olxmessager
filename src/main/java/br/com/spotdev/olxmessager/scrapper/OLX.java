package br.com.spotdev.olxmessager.scrapper;

/**
 * @Descri��o Armazena constantes de URLs de apis do OLX
 * 
 * @Autor Davi Salles
 * @Data 14/04/2016
 */

public class OLX {

	public static final String URL_REGISTER = "https://www3.olx.com.br/account/do_register";
	public static final String URL_LOGIN = "https://www3.olx.com.br/account/do_login";
	public static final String FORM_LOGIN = "https://www3.olx.com.br/account/form_login/";
	public static final String URL_CHAT = "https://chatapi.olx.com.br/chats";
	public static final String URL_MESSAGE = "https://chatapi.olx.com.br/chats/%s/messages";
	
}
