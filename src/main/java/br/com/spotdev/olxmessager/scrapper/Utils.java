package br.com.spotdev.olxmessager.scrapper;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.text.Normalizer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.Random;

import org.apache.http.HttpRequest;
import org.apache.http.ParseException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import br.com.spotdev.olxmessager.scrapper.connection.Chat;
import br.com.spotdev.olxmessager.scrapper.domainfilter.DomainSearch;
import br.com.spotdev.olxmessager.scrapper.enums.ErrorCode;
import br.com.spotdev.olxmessager.scrapper.excpetions.OLXException;
import br.com.spotdev.olxmessager.scrapper.webwhatsapp.WhatsAPPSearch;

/**
 * @Descri��o Fun��es �teis para gera��o de hashs e outras coisas
 * 
 * @Autor Davi Salles
 * @Data 14/04/2016
 */

public class Utils {
	
	/**
	 * Encurta uma URL
	 */
	
	public static String shortUrl(String urlToShort){
		
		try {
			String url = URLEncoder.encode(urlToShort,"UTF-8");
			Document document = Jsoup.connect(
					"https://psbe.co/API.asmx/CreateUrl?real_url="+url).get();
			return document.getElementsByTag("ShortUrl").get(0).text();
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (Exception e){
			return null;
		}
		return null;
	}
	
	/**
	 * Pega o token de registro da aplicação
	 */
	
	public static String generateToken(String email, String password) {
		email = email.toLowerCase(Locale.ENGLISH);
    	String encoded = java.util.Base64.getEncoder().encodeToString(
    			String.format("%s:%s", new Object[]{email, password})
    			.getBytes());
        return ("Basic " + encoded).replace("\n", "");
    }
	
	/**
	 * @description Retorna o ID do chat de um produto
	 */
	public static Chat getChat(CloseableHttpClient httpclient, String productUrl) throws ParseException, IOException, Exception, OLXException{
		
		Chat chat = new Chat();
		String listId = getListIdFromUrl(productUrl);
		HttpPost httpPost = new HttpPost(OLX.URL_CHAT);
		setOLXJSONHeaders(httpPost, productUrl);
 
		StringEntity params = new StringEntity("{\"listId\":"+listId+"}");
		httpPost.setEntity(params);
		
		CloseableHttpResponse response = httpclient.execute(httpPost);
		
		String json = EntityUtils.toString(response.getEntity());
		response.getEntity().getContent().close();
		response.close();
		
		httpPost = null;
		response = null;
		JSONObject obj = null;

		try{
			obj = new JSONObject(json);
		
			String chatid = obj.getJSONObject("data").getString("id");
			int sellerid = obj.getJSONObject("data").getJSONObject("seller").getInt("accountId");
			
			chat.setListid(listId);
			chat.setChatid(chatid);
			chat.setSellerid(sellerid);
			return chat;
		}catch(Exception e){
			System.out.println("ALGO ERRADO COM JSON RECEBIDO: "+json);
			if(obj != null && obj.has("code")){
				String mes = obj.getString("message");
				int cod = obj.getInt("code");
				OLXException olxe = new OLXException(mes, ErrorCode.valueOf(cod));
				throw olxe;
			}else{
				OLXException olxe = new OLXException("getChat() UNAUTHORIZED: "+e.getMessage(), 
						ErrorCode.UNAUTHORIZED);
				throw olxe;
			}
		}
	}
	
	/**
	 * 
	 * @description Seta em um request os headers necess�rios para o OLX
	 * 
	 */
	public static void setOLXJSONHeaders(HttpRequest request, String productUrl){
		request.addHeader("Accept-Language", "pt-BR,pt;q=0.8,en-US;q=0.6,en;q=0.4");
		request.addHeader("User-Agent", "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/45.0.2454.85 Safari/537.36");
		request.addHeader("Accept", "application/json, text/plain, */*");
		request.addHeader("Referer", productUrl);
		request.addHeader("Host", "chatapi.olx.com.br");
	}
	
	/**
	 * Extraí o número de telefone de uma URL utilizando o ID do anúncio
	 * @param annId ID do anúncio
	 * @return Retorna o número de telefone sem traços nem parênteses
	 * @throws IOException 
	 * @throws InterruptedException 
	 */
	
	public static String getTelefone(String annId) throws IOException, InterruptedException{
    	String urlString = "https://nga.olx.com.br/api/v1.1/"
    			+ "public/ads/"+annId+
    			"/phone?lang=pt";
        
	    BufferedReader reader = null;
	    try {
	        URL url = new URL(urlString);
	        URLConnection urlConnection = url.openConnection();
	        urlConnection.setConnectTimeout(60000);
	        urlConnection.setReadTimeout(60000);
	        reader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
	        StringBuffer buffer = new StringBuffer();
	        int read;
	        char[] chars = new char[1024];
	        while ((read = reader.read(chars)) != -1)
	            buffer.append(chars, 0, read); 

	    	JSONObject object = new JSONObject(buffer.toString());
	    	JSONArray phones = object.getJSONArray("phones");
	    	String ocreaded = phones.optJSONObject(0).getString("value");
	    	url = null;
	    	urlConnection = null;
	    	buffer = null;
	    	object = null;
	    	return ocreaded;
	    }catch(NullPointerException e){}catch(Exception e){
	    	System.out.println("Não conseguiu ler "+urlString);
	    }finally {
	        if (reader != null)
	            reader.close();
	        	
	    }

	    return null;
	}
	
	/**
	 * 
	 * @description Seta em um request os headers necessários para requisições
	 * 
	 */
	public static void setOLXHeaders(HttpRequest request, String url){
		request.addHeader("User-Agent", "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/45.0.2454.85 Safari/537.36");
		request.addHeader("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8");
		request.addHeader("Referer", url);
		request.addHeader("Host", "www3.olx.com.br");
		request.addHeader("Origin", "https://www3.olx.com.br");
	}
	
	/**
	 * 
	 * @description Retora o id do anuncio de uma url de anuncio
	 * 
	 */
	public static String getListIdFromUrl(String productUrl){
		return productUrl.split("-")[productUrl.split("-").length-1];
	}
	
	/**
	 * Remove os acentos de uma string
	 */
	public static String removerAcentos(String str) {
	    return Normalizer.normalize(str, Normalizer.Form.NFD).replaceAll("[^\\p{ASCII}]", "");
	}
	
	/**
	 * Formata a mensagem para poder tentar ficari insrrastreável
	 */
	public static String formatUntraceable(String message){
		
		// Procura URL na mensagem e encurta ela
		DomainSearch dSearcher = new DomainSearch(message);
		message = dSearcher.getShorteredText();
		
        WhatsAPPSearch wSearcher = new WhatsAPPSearch(message);
        message = wSearcher.getShorteredText();

        
        StringBuilder builder = new StringBuilder(message);
        if(message.length() >= 20){
    		// Adiciona um espaço a mais para a mensagem não conflitar com bloqueios 
    		// de antes de 24/01/2017
    		message = message.replaceAll("( )(?=[A-Za-z.,])", " "+((char)160));
    
    		// Adiciona um espaço a mais em algum lugar aleatório
    		int messageLenght = builder.length();
    		int indexOf = builder.indexOf("( )", new Random().nextInt(messageLenght));
    		if(indexOf != -1){
    			builder.insert(indexOf, ((char)160));
    		}
    
    		int hebraicoLength = 4;
    		builder.insert(0, getHebraico(hebraicoLength)+" ");
    		
    		if(!message.trim().endsWith(":"))
    		    builder.append(" "+getHebraico(hebraicoLength));
        }
    		
		return builder.toString();
	}
	
	public static String getHebraico(int length){
	    return getHebraico(length, false);
	}
	
	public static String getHebraico(int length, boolean onlyLetters){
	       // Adiciona caracteres especiais no inicio e no final da mensagem para torna-la única
//        char[] invisibleChars = {187,186,185,447,566,359,145,46,168,684,631,693,692,694,697};
	    Character[] numbers = {'1','2','3','4','5','6',
	            '7','8','9','0'};
	    Character[] letters = {'a','b','c','d','e','f','g','h','i','j','k','l'
	            ,'m','n','o','p','q','r','s','t','u','v','w','x','y','z'};
	    
	    List<Character> all = new ArrayList<>();
	    
	    all.addAll(Arrays.asList(letters));
	    if(!onlyLetters)
	        all.addAll(Arrays.asList(numbers));
	    
        int size = all.size();
        
        String hebraico = "";
        for(int x = 0; x < length; x++){
            int max = size;
            if(x == 0 || x == length-1)
                max = 4;
            int chosed = new Random().nextInt(max);
            char charChosed = all.get(chosed);
            hebraico+=charChosed;
        }
        
        return hebraico;

	}
	
	/**
	 * 
	 * @description Gera a hash para ser o id de uma mensagem
	 * 
	 */
	
	public static String generateMessageId(){
		String messageId = generateHash()+generateHash();
	    for(int i = 0; i < 4; i++){
	    	messageId+="-"+generateHash();
	    }
		messageId+= generateHash()+generateHash();
		
		return messageId;
	}
	
	/**
	 * 
	 * @description Gera um peda�o do valor da hash do id da mensagem
	 * 
	 */
	
	private static String generateHash(){
		return Integer.toString((int)Math.floor((1 + Math.random()) * 65536),16).substring(1);
	}
	
}
