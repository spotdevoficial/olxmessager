package br.com.spotdev.olxmessager.scrapper.account;

import java.util.concurrent.ConcurrentLinkedQueue;

public class AbstractAccountSeedModel {

	protected String seed = null;
	protected ConcurrentLinkedQueue<Account> accounts = new ConcurrentLinkedQueue<>();
	protected String password = null;
	
	public ConcurrentLinkedQueue<Account> getAccounts() {
		return accounts;
	}
	public String getSeed() {
		return seed;
	}
	public void setSeed(String seed) {
		this.seed = seed;
	}
	public void setAccounts(ConcurrentLinkedQueue<Account> accounts) {
		this.accounts = accounts;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	
}
