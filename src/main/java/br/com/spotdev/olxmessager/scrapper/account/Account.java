package br.com.spotdev.olxmessager.scrapper.account;

import java.util.Date;

import org.apache.http.client.protocol.HttpClientContext;
import org.apache.http.impl.client.CloseableHttpClient;

/**
 * Classe para criação do objeto de uma conta
 * de usuário criada
 * @author Davi Salles
 * @date 17/05/2016
 */
public class Account {

	// Variáveis privadas do objeto
	private String email = null;					// E-mail do usuário
	private String password = null;					// Password do usuário
	private Date date = null;						// Data do crime
	private CloseableHttpClient httpclient = null;	// Cliente HTTP fechável
	private HttpClientContext httpContext = null;	// Http context
	private String recaptcha = null;				// Token do recaptcha
	private boolean stopped;
	private int envios = 0;
	
	public Account(String email, String password){
		this.email = email;
		this.password = password;
		this.date = new Date();
		new FormRecaptchaAccountGenerator().show(this);
	}


	
	
	public int getEnvios() {
		return envios;
	}




	public void addEnvios() {
		this.envios++;
	}




	public String getRecaptcha() {
		return recaptcha;
	}


	public void setRecaptcha(String recaptcha) {
		this.recaptcha = recaptcha;
	}


	public String getEmail() {
		return email;
	}
	protected void setEmail(String email) {
		this.email = email;
	}
	public String getPassword() {
		return password;
	}
	protected void setPassword(String password) {
		this.password = password;
	}
	public Date getDate() {
		return date;
	}
	protected void setDate(Date date) {
		this.date = date;
	}

	public CloseableHttpClient getHttpclient() {
		return httpclient;
	}

	public void setHttpclient(CloseableHttpClient httpclient) {
		this.httpclient = httpclient;
	}

	public HttpClientContext getHttpClientContext() {
		return httpContext;
	}

	public void setHttpClientContext(HttpClientContext httpContext) {
		this.httpContext = httpContext;
	}
	
	
	
	public synchronized boolean isStopped() {
		return stopped;
	}

	public synchronized void stop(){
		this.date = null;
		this.email = null;
		this.httpclient = null;
		this.httpContext = null;
		this.password = null;
		this.recaptcha = null;
		stopped = true;
	}
	
}
