package br.com.spotdev.olxmessager.scrapper.account;

import java.util.Random;

public class AccountSeed extends AbstractAccountSeedModel {

	private static final int MAX_EMAIL_SIZE = 20;
	
	public AccountSeed(String seed, String password){
		this.setPassword(password);
		this.setSeed(seed);
	}

	public synchronized Account nextAccount(){
		Account account = null;
		while((account = getAccounts().poll()) == null);
		System.out.println("Iniciando envio na conta "+account.getEmail());
		return account;
	}
	
	public void addAccount(Account account){
		this.getAccounts().add(account);
	}

	public Account generateAccount(){
//		int seedSize = seed.length();
//		String email = seed+randomString(MAX_EMAIL_SIZE-seedSize);
        String email = randomString(4+(int)(Math.random()*MAX_EMAIL_SIZE));
		email+="@hotmail.com";
		Account account = new Account(email, getPassword());
		return account;
	}
	
	String randomString(final int length) {
		StringBuilder sb = new StringBuilder();
		boolean consoante = true;
		for (int i = 0; i < length; i++) {
		    
		    char c = consoante ? randomLetter(Tipo.CONSOANTE) : 
		        randomLetter(Tipo.VOGAL) ;
		    consoante = !consoante;
//		    if(i == 0)
//		    	c = '_';
		    
		    sb.append(c);
		}
		String output = sb.toString();
		return output;
	}
	
	enum Tipo {CONSOANTE, VOGAL}
	char randomLetter(Tipo tipo){
	        Random random = new Random();
	       char[] consoantes = "bcdfghjlmnpqrstvz".toCharArray();
	       char[] vogais = "aeiou".toCharArray();
	       
	       char result = 0;
	       switch(tipo){
	           case CONSOANTE:
	               result = consoantes[random.nextInt(consoantes.length)];
	               break;
	           case VOGAL:
                   result = vogais[random.nextInt(vogais.length)];
	               break;
	       }
	       
	       return result;
	}
	

}
