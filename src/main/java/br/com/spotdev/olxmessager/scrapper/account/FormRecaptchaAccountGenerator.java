package br.com.spotdev.olxmessager.scrapper.account;

import br.com.spotdev.olxmessager.anticaptcha.*;
import br.com.spotdev.olxmessager.scrapper.OLX;

public class FormRecaptchaAccountGenerator {

	private Account account = null;
    private static String host = "api.anti-captcha.com";
    public static String clientKey = "baf3b29001d3737fe539cb4e319609c1";
    
	public void show(Account account){
		
		this.account = account;
		
		try {
			create();
		} catch (Exception e1) {
			e1.printStackTrace();
		}
		
	    while(account.getRecaptcha() == null){
	    	try {
				Thread.sleep(500);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
	    }
	    System.out.println("Captcha gerado");
		
	}
	
	String solution = null;
	boolean loaded = false;
	private void create() throws Exception{			

		String recaptchaToken = "6LfzbgMTAAAAAKTwxwSOxs8li2wANWYrVxu7QgrD";
		loaded = true;
		
		AnticaptchaTask task1 = null;
		try {
			task1 = AnticaptchaApiWrapper.createNoCaptchaTaskProxyless(
			        host,
			        clientKey,
			        OLX.FORM_LOGIN, //target website
			        recaptchaToken,     //target website Recaptcha key
			        "Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36"
			);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
 
        System.out.println("Aguardando anticaptcha.");
        try {
			Thread.sleep(10_000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
        
        try {
			solution = processTask(task1);
		} catch (InterruptedException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}

		if(solution != null)
			account.setRecaptcha(solution);
		else{
			try {
				create();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
			   
	}
	
	boolean timeoutRecaptcha = false;
	private String processTask(AnticaptchaTask task) throws Exception {
		 
        AnticaptchaResult response = null;
        timeoutRecaptcha = false;
        new Thread(()-> {
        	try {
				Thread.sleep(200000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
        	timeoutRecaptcha = true;
        }).start();
 
        do
        {
        	
        	if(timeoutRecaptcha)
        		break;
        	
            response = AnticaptchaApiWrapper.getTaskResult(host, clientKey, task);
 
            if (response.getStatus().equals(AnticaptchaResult.Status.ready))
            {
                break;
            }
 
            Thread.sleep(1000);
        } while (response != null && response.getStatus().equals(AnticaptchaResult.Status.processing));
 
        if(timeoutRecaptcha && response == null){
        	System.out.println("Ocorreu timeout no recaptcha");
        	return null;
        }
        
        if (response == null || response.getSolution() == null) {
            System.out.println(response);
        } else {
            System.out.println("The answer is '" + response.getSolution() + "'");
        }
 
        return response.getSolution();
    }
	
}
