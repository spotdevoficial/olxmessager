package br.com.spotdev.olxmessager.scrapper.connection;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import br.com.spotdev.olxmessager.scrapper.account.AccountSeed;
import br.com.spotdev.olxmessager.scrapper.enums.Categoria;
import br.com.spotdev.olxmessager.scrapper.enums.Estado;
import br.com.spotdev.olxmessager.scrapper.enums.Tipo;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

public abstract class AbstractConnectionModel {

	/*
	 * Variáveis privadas do objeto
	 */
	protected Tipo tipo = Tipo.TODOS;								// Tipo de anuncio
	protected Categoria categoria = Categoria.TODAS;				// Categoria de anuncio
	protected Estado estado = Estado.SP; /*Estado.BRASIL;*/			// Estado do anuncio
	protected List<String> messages = Collections.synchronizedList(new ArrayList<>());		// Mensagens que serão enviadas
	protected ObservableList<Integer> sellers = FXCollections.observableArrayList();// Vendedores que ja receberam a mensagem
	protected AccountSeed account = null;
	protected boolean sendAllMessagesToOne = false;		
	protected List<Regiao> regioes = new ArrayList<Regiao>();
	protected List<Regiao> subRegioes = new ArrayList<Regiao>();
	protected Regiao regiao = null;
	protected Regiao subRegiao = null;
	protected int actualPage = 1;
	protected int pageSize = 0;
	protected boolean blockRepeated = true;
	
	public boolean isBlockRepeated() {
		return blockRepeated;
	}
	public void setBlockRepeated(boolean blockRepeated) {
		this.blockRepeated = blockRepeated;
	}
	public int getActualPage() {
		return actualPage;
	}
	public void setActualPage(int actualPage) {
		this.actualPage = actualPage;
	}
	public int getPageSize() {
		return pageSize;
	}
	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}
	public AccountSeed getAccount() {
		return account;
	}
	public void setAccount(AccountSeed account) {
		this.account = account;
	}
	public boolean isSendAllMessagesToOne() {
		return sendAllMessagesToOne;
	}
	public void setSendAllMessagesToOne(boolean sendAllMessagesToOne) {
		this.sendAllMessagesToOne = sendAllMessagesToOne;
	}
	public void addMessage(String message){
		messages.add(message);
	}
	public Tipo getTipo() {
		return tipo;
	}
	public void resetRegions(){
		regioes.clear();
		subRegioes.clear();
		regiao = null;
		subRegiao = null;
	}
	
	public void resetSubRegions(){
		subRegioes.clear();
		subRegiao = null;
	}
	public void setTipo(Tipo tipo) {
		this.tipo = tipo;
		resetRegions();
	}
	public Categoria getCategoria() {
		return categoria;
	}
	public void setCategoria(Categoria categoria) {
		this.categoria = categoria;
		resetRegions();
	}
	public Estado getEstado() {
		return estado;
	}
	public void setEstado(Estado estado) {
		this.estado = estado;
		resetRegions();
	}
	public List<String> getMessages() {
		return messages;
	}
	public void setMessages(ArrayList<String> messages) {
		this.messages = messages;
	}
	public ObservableList<Integer> getSellers() {
		return sellers;
	}
	public void setSellers(ObservableList<Integer> sellers) {
		this.sellers = sellers;
	}
	public AccountSeed getAccountSeed() {
		return account;
	}
	public void setAccountSeed(AccountSeed account) {
		this.account = account;
	}
	public Regiao getRegiao() {
		return regiao;
	}
	public void setRegiao(Regiao regiao) {
		this.regiao = regiao;
	}
	public Regiao getSubRegiao() {
		return subRegiao;
	}
	public void setSubRegiao(Regiao subRegiao) {
		this.subRegiao = subRegiao;
	}
	public List<Regiao> getRegioes() {
		return regioes;
	}
	public List<Regiao> getSubRegioes() {
		return subRegioes;
	}	
	
}
