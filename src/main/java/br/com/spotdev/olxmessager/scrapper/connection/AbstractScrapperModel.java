package br.com.spotdev.olxmessager.scrapper.connection;

import java.util.ArrayList;
import java.util.List;

public abstract class AbstractScrapperModel {
	
	protected List<Anuncio> anuncios = new ArrayList<>();
	protected List<String> announceNumbers = new ArrayList<>();
	
	public synchronized List<Anuncio> getAnuncios() {
		return anuncios;
	}
	protected synchronized List<String> getPhonedAnuncios() {
		return announceNumbers;
	}

	public synchronized Anuncio poolAnuncio(){
		
		if(anuncios.size() <= 0)
			return null;
		
		Anuncio anuncio = anuncios.get(0);
		anuncios.remove(0);
		
		return anuncio;
	}
	protected synchronized boolean addAnnounceNumber(String telefone){
		if(!announceNumbers.contains(telefone)){
			announceNumbers.add(telefone);
			return true;
		}
		
		return false;
	}
	
	public synchronized void addAnuncio(Anuncio anuncio){
		anuncios.add(anuncio);
	}
	
}
