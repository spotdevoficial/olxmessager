package br.com.spotdev.olxmessager.scrapper.connection;

/**
 * @Descricao Objeto que representa um chat no OLX
 * 
 * @Autor Davi Salles
 * @Data 30/04/2016
 */
public class Chat {

	// Variáveis do chat
	private String chatid;		// id do chat
	private String listid;		// id do produto
	private int sellerid;	// id do vendedor

	public String getChatid() {
		return chatid;
	}

	public void setChatid(String chatid) {
		this.chatid = chatid;
	}

	public String getListid() {
		return listid;
	}

	public void setListid(String listid) {
		this.listid = listid;
	}

	public int getSellerid() {
		return sellerid;
	}

	public void setSellerid(int sellerid) {
		this.sellerid = sellerid;
	}
	
	
	
}
