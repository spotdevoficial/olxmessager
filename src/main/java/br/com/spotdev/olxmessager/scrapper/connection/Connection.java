package br.com.spotdev.olxmessager.scrapper.connection;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.MalformedURLException;
import java.net.Proxy;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import javax.net.ssl.SSLContext;

import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.CookieStore;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.protocol.HttpClientContext;
import org.apache.http.conn.socket.PlainConnectionSocketFactory;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.cookie.Cookie;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.HttpContext;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import br.com.spotdev.olxmessager.scrapper.OLX;
import br.com.spotdev.olxmessager.scrapper.Utils;
import br.com.spotdev.olxmessager.scrapper.account.Account;
import br.com.spotdev.olxmessager.scrapper.account.AccountSeed;
import br.com.spotdev.olxmessager.scrapper.enums.Categoria;
import br.com.spotdev.olxmessager.scrapper.enums.ErrorCode;
import br.com.spotdev.olxmessager.scrapper.enums.Estado;
import br.com.spotdev.olxmessager.scrapper.enums.Tipo;
import br.com.spotdev.olxmessager.scrapper.events.OLXEnvioEvent;
import br.com.spotdev.olxmessager.scrapper.excpetions.OLXException;
import br.com.spotdev.olxmessager.scrapper.excpetions.SellerAlreadyComunicated;
import br.com.spotdev.olxmessager.scrapper.excpetions.SendingAlreadyStarted;

/**
 * @Descrição Classe para manutenir a conexão com o OLX
 * 
 * @Autor Davi Salles
 * @Data 14/04/2016
 */

public class Connection extends AbstractConnectionModel {
	
	/*
	 * Variáveis privadas do objeto
	 */
	private boolean parado = true;								// Verifica se esta parado ou não o envio
	public static int SECONDS_DELAY = 50;						// Intervalo de envio
	private int enviados;										// Quantidade de mensagens enviadas

	public Connection(String userSeed, String password){
		AccountSeed seed = new AccountSeed(Utils.removerAcentos(userSeed), password);
		this.setAccountSeed(seed);
	}
	
	/**
	 * @description Realiza o login no OLX
	 * 
	 * @param email E-mail do OLX
	 * @param password Password do OLX
	 * @return Retorna se o login foi realizado com sucesso
	 * @throws IOException Excessão caso a conexão falhe
	 */
	HttpPost postLogin = null;
	public boolean login(Account account) throws IOException {
		
		HttpClientContext httpContext = account.getHttpClientContext();
		CloseableHttpClient httpclient = account.getHttpclient();
		
		postLogin = new HttpPost(OLX.URL_LOGIN);
		
		// Adiciona os parametros do post
		List<NameValuePair> nvps = new ArrayList <NameValuePair>();
		nvps.add(new BasicNameValuePair("email", account.getEmail()));
		nvps.add(new BasicNameValuePair("password", account.getPassword()));
		nvps.add(new BasicNameValuePair("entrar", "Entrar"));
		postLogin.setEntity(new UrlEncodedFormEntity(nvps));
		
		Thread t = new Thread(()->{
			try {
				Thread.sleep(3000);
			} catch (InterruptedException e) {
				return;
			}
			if(postLogin != null && !Thread.interrupted())
				postLogin.abort();
		});
		t.start();

		// Executa a conexão
		System.out.println("realizando login");
		CloseableHttpResponse response = 
				httpclient.execute(postLogin, httpContext);
		postLogin = null;
		t.interrupt();
		System.out.println("login realizado");
		return isLogged(response, httpContext);
		
	}
	
	/**
	 * Função para criar contas de usuarios
	 * 
	 * @throws IOException 
	 * @throws ClientProtocolException 
	 */
	public boolean registerNew() throws ClientProtocolException, IOException{
		Account account = getAccountSeed().generateAccount();
		recreateHttpClient(account);
		return register(account, account.getHttpclient(),
				account.getHttpClientContext());
	}
	
	public enum BrowserType {
		PRINCIPAL,
		ASYNC;
	}
	
	/**
	 * Registra uma conta pré-definida
	 * @param account Conta a ser registrada
	 * @throws IOException 
	 */
	HttpPost postRegister = null;
	public boolean register(Account account, 
			CloseableHttpClient httpclient, 
			HttpClientContext httpContext) throws IOException{

		// Pega o cookie da página principal
		HttpGet httpGet = new HttpGet(OLX.URL_LOGIN);
		CloseableHttpResponse getResponse = httpclient.execute(httpGet, httpContext);
		getResponse.close();
		
		// Instancia as conexões
		postRegister = new HttpPost(OLX.URL_REGISTER);
		Utils.setOLXHeaders(postRegister, "https://www3.olx.com.br/account/form_login/");
		
		// Adiciona os parametros do post
		List<NameValuePair> nvps = new ArrayList <NameValuePair>();
		nvps.add(new BasicNameValuePair("return_to", ""));
		nvps.add(new BasicNameValuePair("email", account.getEmail()));
		nvps.add(new BasicNameValuePair("password", account.getPassword()));
		nvps.add(new BasicNameValuePair("password_confirm", account.getPassword()));
		nvps.add(new BasicNameValuePair("g-recaptcha-response", account.getRecaptcha()));
		nvps.add(new BasicNameValuePair("cadastrar", "Criar conta"));
		UrlEncodedFormEntity uef = new UrlEncodedFormEntity(nvps, "UTF-8");
		postRegister.setEntity(uef);
		
		Thread t = new Thread(()->{
			try {
				Thread.sleep(10000);
			} catch (InterruptedException e) {
				return;
			}
			if(postRegister != null && !Thread.interrupted()){
				System.out.println("Register timeout");
				postRegister.abort();
			}
		});
		t.start();

		// Executa a conexão
		System.out.println("cadastrando");
		CloseableHttpResponse response = httpclient.execute(postRegister, httpContext);
		postRegister = null;
		t.interrupt();
		System.out.println("cadastrado "+account.getEmail()+" - "+account.getPassword());
		boolean works = isLogged(response, httpContext);
		if(works){
			getAccountSeed().addAccount(account);
		}
		
		return works;

	}
	
	/**
	 * @description Inicia o envio de mensagens automáticas
	 * 
	 * @param event Evento de quando uma mensagem foi enviada com sucesso
	 * @throws SendingAlreadyStarted Erro de caso já esteja enviando uma campanha
	 */
	public void startEnvio(OLXEnvioEvent event, int startPage) throws SendingAlreadyStarted,
			IndexOutOfBoundsException {
		if(parado == false) throw new SendingAlreadyStarted();
		Scrapper scrapper = new Scrapper();
		parado = false;
		String url = generateScraperUrl();				 // Pega a URL para iniciar o scraper
		int size = scrapper.getPaginationSize(url);		 // Pega o tamanho do pagination do OLX
		final int sizeToUse = size >= 500 ? 500 : size;// Faz o máximo não passar de mil páginas
	
		this.setPageSize(sizeToUse);
		
		// Cria uma thread para envio
		final Thread envio = new Thread(()->{
			scrapper.loopAllPages(startPage,  sizeToUse, url, event, this);	// Faz um loop em todas as páginas
			enviados = 0;
		});
		envio.start();
	}
	
	/**
	 * Para o envio 
	 */
	public void pararEnvio(){
		parado = true;
	}
	
	/**
	 * Envia uma mensagem para um produto no OLX
	 * 
	 * @param productUrl Url do produto que receberá uma mensagem
	 * @throws FailingHttpStatusCodeException
	 * @throws MalformedURLException
	 * @throws IOException
	 */
	public void sendMessage(Anuncio anuncio, Account account) throws MalformedURLException, 
	IOException, SellerAlreadyComunicated, Exception, OLXException{
		
		CloseableHttpClient httpclient = account.getHttpclient();
	    String messageId = Utils.generateMessageId();					// Gera um id para a mensagem
	    Chat chat = Utils.getChat(httpclient, anuncio.getUrl());
	    
	    if((sellers.contains(chat.getSellerid()) && isBlockRepeated()))
	    	throw new SellerAlreadyComunicated();
	    
	    sellers.add(chat.getSellerid());
		
	    // Cria um post com um json e define os headers necessário para o OLX
		HttpPost httpPost = new HttpPost(String.format(OLX.URL_MESSAGE, chat.getChatid()));
		Utils.setOLXJSONHeaders(httpPost, anuncio.getUrl());	

		if(isSendAllMessagesToOne()){
			for(int i = 0; i < messages.size(); i++){
				String message = messages.get(i);
				if(i != 0)
	                Thread.sleep(Connection.SECONDS_DELAY*1000);
				System.out.println(i+" "+account.getEmail()+"@"+account.getPassword()+" MESSAGEID: "+messageId+" - "+anuncio.getUrl());
				doSend(messageId, httpPost, httpclient, message);
			}
		}else{
			doSend(messageId, httpPost, httpclient, getMessage());
		}
		
	}
	
	/**
	 * Faz o envio
	 * @throws IOException 
	 * @throws ClientProtocolException 
	 */
	private HttpPost httpPost = null;
	private void doSend(String messageId, HttpPost httpPostt, 
			CloseableHttpClient httpclient, String message) 
			        throws ClientProtocolException, IOException, OLXException{
		
		message = Utils.formatUntraceable(message);
		
		for(int i = 0; i < 3; i++){
			httpPost = httpPostt;
			JSONObject obj = new JSONObject();
			obj.put("type","TEXT");
			obj.put("text",message);
			obj.put("id",messageId);
			String request = obj.toString();
			  
			StringEntity params = new StringEntity(request, "UTF-8");
			httpPost.setEntity(params);
			
//			Thread t = new Thread(()->{
//				try {
//					Thread.sleep(5000);
//				} catch (InterruptedException e) {
//					return;
//				}
//				if(httpPost != null && !Thread.interrupted())
//					httpPost.abort();
//			});
//			t.start();
			
			// Executa a conexão
			try{
			    System.out.println("ENVIANDO: "+messageId);
				CloseableHttpResponse response = httpclient.execute(httpPost);
				System.out.println("RESPONSE: "+messageId+" - "+response.getStatusLine().getStatusCode()+" "
				        +EntityUtils.toString(response.getEntity()));
//				t.interrupt();
				int code = response.getStatusLine().getStatusCode();
                response.getEntity().getContent().close();
                response.close();
                
                if(code != 201)
                    throw new OLXException("Unreachable", ErrorCode.UNKNOW);
                
				httpPost = null;
				response = null;
				
				break;
			}catch(Exception e){
			    System.out.println("Erro: "+messageId+" : "+e.getMessage());
			    e.printStackTrace();
//				t.interrupt();
				if(i < 2){
					System.out.println("Ocorreu um exception, retentando envio "+e.getMessage()+" "+e.getClass());
					continue;
				}else{
					throw e;
				}
			}
		}
	}
	
	public List<Regiao> getRegions(String url) throws IOException{

		try {
			Document doc = Jsoup.connect(url).timeout(0).userAgent("Mozilla").get();
			Elements elementsRegions = doc.select(".page_listing .section_linkshelf .linkshelf-tabs .linkshelf-tabs-content .list .item .link");
			resetRegions();
			Regiao regiaoNenhuma = new Regiao();
			regiaoNenhuma.setUrl(null);
			regiaoNenhuma.setNome("TODAS");
			this.regioes.add(regiaoNenhuma);
			
			for(Element region : elementsRegions){
				Regiao regiaoNova = new Regiao();
				regiaoNova.setUrl(region.attr("href"));
				regiaoNova.setNome(region.text());
				this.regioes.add(regiaoNova);
			}
		} catch (IOException e) {
			e.printStackTrace();
			throw e;
		}
		
		return regioes;
	}
	
	public List<Regiao> getSubregions(){
		
		if(regiao == null)
			return null;
		
		try {
			Document doc = Jsoup.connect(regiao.getUrl()).timeout(0).userAgent("Mozilla").get();
			Elements elementsRegions = doc.select(".page_listing .section_linkshelf .linkshelf-tabs .linkshelf-tabs-content .list .item .link");
			resetSubRegions();
			
			Regiao regiaoNenhuma = new Regiao();
			regiaoNenhuma.setUrl(regiao.getUrl());
			regiaoNenhuma.setNome("TODAS");
			this.subRegioes.add(regiaoNenhuma);
			
			for(Element region : elementsRegions){
				Regiao regiaoNova = new Regiao();
				regiaoNova.setUrl(region.attr("href"));
				regiaoNova.setNome(region.text());
				this.subRegioes.add(regiaoNova);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return subRegioes;
	}
	
	/**
	 * @return Retorna uma mensagem aleatoria das adicionadas
	 */
	String lastMessage = "";
	public String getMessage(){
		StringBuilder builder = randomMessage();
		lastMessage = builder.toString();

		return builder.toString();
	}
	
	private StringBuilder randomMessage(){
		Random r = new Random(); 
		StringBuilder builder = new StringBuilder();
		while(builder.length() <=0 || builder.toString().equals(lastMessage)){
			if(messages.size() == 1){
				builder.append(messages.get(0));
				break;
			}
			
			int value = r.nextInt(messages.size());
			builder.setLength(0);
			builder.append(messages.get(value));
		}
		
		return builder;
	}
	
	/**
	 * @return Retorna a URL da combinação de categoria, tipo e estado do OLX
	 */
	public String generateScraperUrl(){
		
		String tipo = "?"+getTipo().getValue();
		
		if(subRegiao != null && subRegiao.getUrl()!= null){
			String urlEnd = "";
			if(getTipo() == Tipo.TODOS)
				urlEnd = "?";
			return subRegiao.getUrl()+urlEnd;
		}
		
		if(regiao != null && regiao.getUrl() != null)
			return regiao.getUrl()+tipo;
		
		String state = getEstado() == Estado.BRASIL ? "www" : getEstado().toString().toLowerCase();
		state+=".";
		String categoria = getCategoria().getURL();
		if(getEstado() == Estado.BRASIL && getCategoria() == Categoria.TODAS)
			categoria = "brasil";
		String url = "http://"+state+"olx.com.br/"+categoria+tipo;
		
		return url;
	}
	
	/**
	 * Define os httpclients sempre que precisar
	 * @throws IOException
	 */
	public void recreateHttpClient(Account account) throws IOException{
		CloseableHttpClient httpclient = HttpClients.createDefault();
		HttpClientContext httpContext = HttpClientContext.create();
		account.setHttpclient(httpclient);
		account.setHttpClientContext(httpContext);
	}
	
	/**
	 * Verifica se o usuário ta logado por cookies
	 * @param response Resposta do httpclient
	 * @return boolean Retorna se á esta logado ou não
	 * @throws IOException
	 */
	private boolean isLogged(CloseableHttpResponse response, HttpClientContext httpContext) throws IOException{
		// Verifica se foi conectado com sucesso
		try {
		    
		    CookieStore cookies = httpContext.getCookieStore();
		    for(Cookie c : cookies.getCookies()){
		    	if(c.getName().equals("loginIdentifier")){
		    		
		    		/**
		    		 * SUCESSO
		    		 */
		    		return true;
		    	}
		    }
		    
		} finally {
			response.getEntity().getContent().close();
		    response.close();
		}
		
		return false;
	}
	
    static class MyConnectionSocketFactoryHTTP extends PlainConnectionSocketFactory {

        @Override
        public Socket createSocket(final HttpContext context) throws IOException {
            InetSocketAddress socksaddr = (InetSocketAddress) context.getAttribute("socks.address");
            Proxy proxy = new Proxy(Proxy.Type.SOCKS, socksaddr);
            return new Socket(proxy);
        }

    }
    
    static class MyConnectionSocketFactory extends SSLConnectionSocketFactory {

        public MyConnectionSocketFactory(final SSLContext sslContext) {
            super(sslContext);
        }
    }
	
	/**
	 * Getters and setters
	 */

	public boolean isParado() {
		return parado;
	}

	public void setParado(boolean parado) {
		this.parado = parado;
	}

	public int getEnviados() {
		return enviados;
	}

	public void setEnviados(int enviados) {
		this.enviados = enviados;
	}
	
	
}
