package br.com.spotdev.olxmessager.scrapper.connection;

import java.util.Date;

import br.com.spotdev.olxmessager.scrapper.account.Account;
import br.com.spotdev.olxmessager.scrapper.enums.ErrorCode;
import br.com.spotdev.olxmessager.scrapper.excpetions.OLXException;
import br.com.spotdev.olxmessager.scrapper.excpetions.SellerAlreadyComunicated;

/**
 * @Descri��o Comando de selecionar a categoria
 * 
 * @Autor Davi Salles
 * @Data 14/04/2016
 */

public class Envio {

	// Variaveis privadas do objeto
	private String mensagem = null;					// Mensagem
	private Date date = null;						// Data de envio
	private boolean sucesso = true;					// Se foi um sucesso ou n�o
	private Chat chat;								// Chat onde o envio foi realizado
	private ErrorCode errorCode	= ErrorCode.UNKNOW;	// Código do erro
	private Account account = null;
	private Anuncio anuncio = null;
	
	protected void sucesso(String message){
		this.setSucesso(true);
		this.setMensagem(message);
		this.setDate(new Date());
	}
	
	protected void error(Exception ex){
		this.setSucesso(false);
		this.setDate(new Date());
		if(ex instanceof SellerAlreadyComunicated){
			this.setMensagem("Este vendedor já foi comunicado");
			this.setErrorCode(ErrorCode.SELLER_ALREADY_COMMUNICATED);
		}else if(ex instanceof OLXException){
			this.setErrorCode(((OLXException)ex).getCode());
			this.setMensagem("O Olx retornou um erro!\n\t"+ex.getMessage());
		}else {
			this.setMensagem("Ocorreu um erro desconhecido: \n\t"+ex.getMessage());
		}
	}
	
	
	public Account getAccount() {
		return account;
	}

	public void setAccount(Account account) {
		this.account = account;
	}

	public Chat getChat() {
		return chat;
	}
	public void setChat(Chat chat) {
		this.chat = chat;
	}
	public String getMensagem() {
		return mensagem;
	}
	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}
	public Date getDate() {
		return date;
	}
	public boolean isSucesso() {
		return sucesso;
	}
	
	public Anuncio getAnuncio() {
		return anuncio;
	}

	public void setAnuncio(Anuncio anuncio) {
		this.anuncio = anuncio;
	}

	protected void setDate(Date date) {
		this.date = date;
	}
	protected void setSucesso(boolean sucesso) {
		this.sucesso = sucesso;
	}
	public ErrorCode getErrorCode() {
		return errorCode;
	}
	protected void setErrorCode(ErrorCode errorCode) {
		this.errorCode = errorCode;
	}
	
	
	
}
