package br.com.spotdev.olxmessager.scrapper.connection;

import java.io.IOException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import br.com.spotdev.olxmessager.scrapper.Utils;
import br.com.spotdev.olxmessager.scrapper.account.Account;
import br.com.spotdev.olxmessager.scrapper.enums.ErrorCode;
import br.com.spotdev.olxmessager.scrapper.events.OLXEnvioEvent;
import br.com.spotdev.olxmessager.scrapper.excpetions.OLXException;
import br.com.spotdev.olxmessager.scrapper.excpetions.SellerAlreadyComunicated;

class Scrapper extends AbstractScrapperModel {

	/**
	 * Faz um loop em todos os produtos da página do OLX
	 * @param elementsProducts
	 * @param event
	 * @param connection
	 */
	static boolean firstEnvio = true;
	protected void loopAllProducts(OLXEnvioEvent event, Connection connection){
		
		// Faz um loop em todos os produtos de uma página
		Anuncio anuncio = null;
		System.out.println("Enviando por thread");
		Account account = null;
		account = connection.getAccountSeed().nextAccount();

		while((anuncio = poolAnuncio()) != null){ 
			connection.setActualPage(anuncio.getPage());

			// Cria um objeto de erro para caso o vendedor ja 
			// tenha sido comunicado
			Envio envioObject = new Envio();
			envioObject.setAccount(account);
			envioObject.setAnuncio(anuncio);
			try{
				// Tenta enviar uma mensagem
				connection.sendMessage(anuncio, account);
				connection.setEnviados(connection.getEnviados()+1);
				account.addEnvios();
				envioObject.sucesso("Mensagem enviada com sucesso na página "+envioObject.getAnuncio().getPage());
				checkifIsFirst(event, connection);
				synchronized (this) {event.envioRealizado(envioObject);}
				try {Thread.sleep((long)(Connection.SECONDS_DELAY*1000));} 
				catch (Exception e) {}
			}catch(Exception sac){
				// Falha ao enviar mensagem
				if(!(sac instanceof SellerAlreadyComunicated))
					sac.printStackTrace();
				envioObject.error(sac);
				synchronized (this) {event.envioRealizado(envioObject);}
				
				if(sac instanceof OLXException){
					if(((OLXException)sac).getCode() == ErrorCode.UNAUTHORIZED){
						System.out.println("UNAUTHORIZED EM "+account.getEmail());
						try {
							Thread.sleep(5000);
						} catch (InterruptedException e) {
							e.printStackTrace();
						}
					}
				}
				
			}finally {
				if(connection.isParado() == true){
					break;
				}
				
				if(account.isStopped())
					account = connection.getAccountSeed().nextAccount();
			}
			
			
		}
	}
	
	private synchronized void checkifIsFirst(OLXEnvioEvent event, Connection conn){
		if(firstEnvio){
			event.envioIniciado(conn);
			firstEnvio = false;
		}
	}
	
	/**
	 * Faz um loop em todos os produtos da página do OLX e os salvo
	 * @param elementsProducts
	 * @param event
	 * @param connection
	 */

	protected void loopAndSaveAllProducts(Elements elementsProducts, Connection connection, int page){
		
		// Faz um loop em todos os produtos de uma página
		ExecutorService executor = Executors.newFixedThreadPool(4);
		
		for(final Element produtoElement : elementsProducts){
			
			// Abre uma thread para cada request
			executor.submit(()->{
				Anuncio anuncio = null;
				String annUrl = produtoElement.attr("href");
				
				// Gera a instância do anúncio
				anuncio = new Anuncio();
		   		anuncio.setUrl(annUrl);
		   		anuncio.setPage(page);
		   		String anuncioId = Utils.getListIdFromUrl(annUrl);
		   		String telefone = null;
				try {
					telefone = Utils.getTelefone(anuncioId);
				} catch (IOException e) {
					e.printStackTrace();
				} catch (InterruptedException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
		   		if(telefone == null || this.addAnnounceNumber(telefone)){
		   			this.addAnuncio(anuncio);
		   		}
			});
	
		}
		
		// Não deixa a função prosseguir enquanto não terminar de escaniar os anuncios
		try{
			executor.shutdown();
			executor.awaitTermination(Long.MAX_VALUE, TimeUnit.NANOSECONDS);
			executor = null;
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	/**
	 * Faz um loop em todas as páginas do OLX
	 * @param size Quantidade de páginas que fara o loop
	 * @param url Url da categoria
	 * @param event Evento do loop
	 * @param connection Conexão em que o loop será realizado
	 */

	int pageCount = 0;
	protected void loopAllPages(int startPage, int size, String url, OLXEnvioEvent event, 
			Connection connection){
		long now = System.currentTimeMillis();
		ExecutorService executor = Executors.newFixedThreadPool(10);
		// Inicia pegar todas as páginas do pagination
		for(int page = startPage; page <= size; page++){
			final int actualPage = page;
			executor.submit(()->{
				
				try {
					String listPageUrl = url+"&o="+actualPage;
					Document doc = Jsoup.connect(listPageUrl).timeout(0).userAgent
							("Mozilla").get();
					Elements elementsProducts = doc.select(".OLXad-list-link");
					loopAndSaveAllProducts(elementsProducts, connection, actualPage);	
					doc = null;			
				} catch (Exception e) {
					e.printStackTrace();
				}
				pageCount++;
			});
		}
		
		final int finalSize = size;
		new Thread(()->{
			while(pageCount+startPage<finalSize){
				try {
					Thread.sleep(2000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
		    	System.gc();	
				System.out.println("Extração de páginas em "+((double)((double)pageCount+startPage)/finalSize)*100+"%. ");
			}
		}).start();
		
		try {
			executor.shutdown();
			executor.awaitTermination(Long.MAX_VALUE, TimeUnit.NANOSECONDS);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		getPhonedAnuncios().clear();
		System.out.println("Encerrado scam em "+(System.currentTimeMillis()-now)+" milésimos com "+getAnuncios().size()+" anúncios.");

		// Utiliza o delay para a quantidade de threads para enviar uma mensagem por segundo
		executor = Executors.newFixedThreadPool(5);
		for(int i = 0; i < 5; i++){
			executor.submit(()->{
				System.out.println("Iniciando thread");
				loopAllProducts(event, connection);
			});
		}
		try {
			executor.shutdown();
			executor.awaitTermination(Long.MAX_VALUE, TimeUnit.NANOSECONDS);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		event.envioFinalizado(connection);
	}
	
	/**
	 * 
	 * @description Adquiri o tamanho do pagination de uma URL do olx
	 * 
	 */
	
	public int getPaginationSize(String url) throws IndexOutOfBoundsException {
		Document docSizer = null;
		try {
			docSizer = Jsoup.connect(url).timeout(0).userAgent("Mozilla").get();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		Element maxSize = docSizer.select(".module_pagination .item.last > a").get(0);
		String maxString = maxSize.attr("href");
		maxString = maxString.substring(maxString.indexOf("o="), maxString.length());
		maxString = maxString.replaceAll("o=", "");
		
		int pagesSize = 0;
		try{
			pagesSize = Integer.parseInt(maxString);
		}catch(Exception e){
			e.printStackTrace();
		}
		
		return pagesSize;
	}
	
}
