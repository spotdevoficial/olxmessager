package br.com.spotdev.olxmessager.scrapper.domainfilter;

import java.util.ArrayList;
import java.util.List;

abstract class AbstractDomainSearch {

	/**
	 * Lista de variáveis privadas
	 */
	private List<DomainFound> domainFounds = new ArrayList<>();	// URLs encontradas no texto
	private String text = null;									// Texto em que a busca foi realizada
	
	/**
	 * Construtor da classe
	 * @param text string em que será buscado os domínios
	 */
	public AbstractDomainSearch(String text){
		this.text = text;
	}
	
	/**
	 * GETTERS AND SETTERS
	 */
	
	
	public String getText() {
		return text;
	}

	public List<DomainFound> getDomainFounds() {
		return domainFounds;
	}
	
	
}
