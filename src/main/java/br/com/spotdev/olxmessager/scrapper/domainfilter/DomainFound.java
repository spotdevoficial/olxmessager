package br.com.spotdev.olxmessager.scrapper.domainfilter;

import java.net.URI;

import br.com.spotdev.olxmessager.scrapper.Utils;

/**
 * @Descrição Classe que armazena um domínio encontrado
 * 
 * @Autor Davi Salles
 * @Data 24/01/2016
 */
public class DomainFound {

	/**
	 * Variáveis privadas da classe
	 */
	private URI urlFound = null;	// URL Encontrada
	private URI shorteredUrl = null;	// URL encurtada
	private int fromIndex = -1;		// Index em que a URL inicia na mensagem
	private int toIndex = -1;		// Index em que a URL termina na mensagem
	
	
	/**
	 * GETTERS AND SETTERS
	 */
	
	public URI getUrlFound() {
		return urlFound;
	}
	public void setUrlFound(URI urlFound) {
		this.urlFound = urlFound;
	}
	public int getFromIndex() {
		return fromIndex;
	}
	public void setFromIndex(int fromIndex) {
		this.fromIndex = fromIndex;
	}
	public int getToIndex() {
		return toIndex;
	}
	public void setToIndex(int toIndex) {
		this.toIndex = toIndex;
	}
	public String getShorteredUrl() {
		return Utils.getHebraico(3)+" "+shorteredUrl;
	}
	public void setShorteredUrl(URI shorteredUrl) {
		this.shorteredUrl = shorteredUrl;
	}
	
	public void dispose(){
		this.shorteredUrl = null;
		this.urlFound = null;
	}
	
	@Override
	public String toString(){
		return getUrlFound().toString();
	}
	
}
