package br.com.spotdev.olxmessager.scrapper.domainfilter;

import java.net.URI;
import java.net.URISyntaxException;

import org.apache.commons.lang3.StringUtils;

/**
 * @Descrição Classe que busca por domínios em textos
 * 
 * @Autor Davi Salles
 * @Data 24/01/2016
 */
public class DomainSearch extends AbstractDomainSearch {

	public DomainSearch(String text) {
		super(text);
		search();
	}

	/**
	 * Função que inicia a busca 
	 * 
	 * @return true caso tenha encontrado algo
	 */
	private boolean search(){
		clearDomains();
		int extensionIndex = -1;
		String stringToSearch = getText().toLowerCase();
		int lastIndexFound = 0;
		
		// Inicia a busca por todas as extensões de domínios
		loopSearcher: while(lastIndexFound != -1){

			lastIndexFound = indexOfByLoop(stringToSearch, 
					DomainExtensions.domains_extensions, lastIndexFound);
			
			
			if(lastIndexFound == -1)
				continue;
			
			extensionIndex = lastIndexFound;
			lastIndexFound++;
			
			int firstDomainIndex = 0;
			int lastDomainIndex = stringToSearch.length();
			
			// Caso a extensão de domínio não tenha domínio pula
			if(Character.isWhitespace(stringToSearch.charAt(extensionIndex-1)))
				continue;

			// Busca pelo inicio do domínio
			for(int i = extensionIndex; i >= 0; i--){
				char charAt = stringToSearch.charAt(i);
			
				if(Character.isWhitespace(charAt)){
					firstDomainIndex = i+1;
					break;
				}else if(charAt == '@'){
				    continue loopSearcher;
				}
			}
			
			// Busca pelo final do domínio
			for(int i = extensionIndex; i <= stringToSearch.length()-1; i++){
				char charAt = stringToSearch.charAt(i);

				if(Character.isWhitespace(charAt)){
					lastDomainIndex = i;
					break;
				}
			}

			lastIndexFound = lastDomainIndex;
			// Confere se o domínio encontrado pode se tornar uma uri
			String uriString = getText().substring(firstDomainIndex, lastDomainIndex);
			uriString = trimNotLetterAndNumber(uriString);
			if(!uriString.startsWith("http://") && !uriString.startsWith("https://"))
				uriString = "http://"+uriString;
						
			try {

				URI uri = new URI(uriString);
				DomainFound found = new DomainFound();
				found.setFromIndex(firstDomainIndex);
				found.setToIndex(lastDomainIndex);
				found.setUrlFound(uri);
				found.setShorteredUrl(uri);
				
				this.getDomainFounds().add(found);
			} catch (URISyntaxException e) {
				e.printStackTrace();
				break;
			}
		}
		
		return hasDomain();
	}
	
	/**
	 * Retorna se a última busca tinha domínio
	 * 
	 * @return falso caso não tenha domínios
	 */
	public boolean hasDomain(){
		return getDomainFounds().size() <= 0;
	}
	
	/**
	 * Limpa a lista de domínios e da dispose nos objetos de dentro
	 */
	private void clearDomains(){
		for(DomainFound domainFound : getDomainFounds()){
			domainFound.dispose();
		}
		
		getDomainFounds().clear();
	}
	
	/**
	 * Retorna o texto encurtado
	 * 
	 * @return o mesmo que o getText mas encurtado
	 */
	
	public String getShorteredText(){
		StringBuilder builder = new StringBuilder(getText());
		int compensador = 0;
		
		for(DomainFound found : getDomainFounds()){
			
			if(found.getShorteredUrl() == null)
				continue;
			
			int sizeUrl = found.getToIndex()-found.getFromIndex();
			int sizeShortered = found.getShorteredUrl().toString().length();
			builder.delete(found.getFromIndex()+compensador, found.getToIndex()+compensador);
			builder.insert(found.getFromIndex()+compensador, found.getShorteredUrl());
			compensador+=sizeShortered-sizeUrl;
		}
		
		return builder.toString();
		
	}
	
	/**
	 * Retorna o texto encurtado
	 * 
	 * @return o mesmo que o getText mas encurtado
	 */
	
	public String getObufuscadText(){
		StringBuilder builder = new StringBuilder(getText());
		int compensador = 0;
		
		for(DomainFound found : getDomainFounds()){
			
			if(found.getUrlFound() == null)
				continue;
			
			String newUrl = found.getUrlFound().toString().
					replace("", "").
					replaceAll("www.", "www. ").
					replaceAll(".com", " .com").
					replaceAll(".top", " .top").
					replaceAll(".tk", " .tk");
			int sizeUrl = found.getToIndex()-found.getFromIndex();
			int sizeShortered = newUrl.toString().length();
			builder.delete(found.getFromIndex()+compensador, found.getToIndex()+compensador);
			builder.insert(found.getFromIndex()+compensador, newUrl);
			compensador+=sizeShortered-sizeUrl;
		}
		
		return builder.toString();
		
	}
	
	/**
	 * Faz loop em uma array rodando a função indexOf
	 * 
	 * @param str String em que será realizada a busca
	 * @param search String buscada
	 * @param index Index de inicio
	 */
	private int indexOfByLoop(String str, String[] search, int index){
		int result = -1;
		for(int i = 0; i < search.length; i++){
			String firstToSearch = search[i];
			int indexOf = str.indexOf(firstToSearch, index);
			
			if((indexOf < result || result == -1) && indexOf != -1){
				result = indexOf;
			}
		}
		
		return result;
	}
	
	/**
	 * Remove de um texto tudo do inicio e do fim que não for letra e numero
	 * @param text texto que passara o trim
	 * @return retorna o texto trim
	 */
	
	private String trimNotLetterAndNumber(String text){
		
		boolean firstOk = false;
		boolean lastOk = false;
		while(!firstOk || !lastOk){
			if(!Character.isLetter(text.charAt(0)) && !Character.isDigit(text.charAt(0))){
				text = StringUtils.substring(text, 1, text.length()-1);
			}else{
				firstOk = true;
			}
			
			char lastChar = text.charAt(text.length()-1);
			if(!Character.isLetter(lastChar) && !Character.isDigit(lastChar)){
				text = StringUtils.substring(text, 0, text.length()-1);
			}else{
				lastOk = true;
			}
		}
		
		return text;
	}
	
}
