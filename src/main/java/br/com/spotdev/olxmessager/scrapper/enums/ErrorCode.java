package br.com.spotdev.olxmessager.scrapper.enums;

import java.util.HashMap;
import java.util.Map;

public enum ErrorCode {

	BANNED_SELLER(200),
	UNAUTHORIZED(401),
	SELLER_ACCOUNT_ID_NOT_FOUND(424),
	UNKNOW(-1),
	SELLER_ALREADY_COMMUNICATED(-2332123);
	
	private int errorCode;
	private static Map<Integer, ErrorCode> map = new HashMap<Integer, ErrorCode>();
	
	ErrorCode(int errorCode){
		this.errorCode = errorCode;
	}
	
	public int getErrorCode(){
		return errorCode;
	}
	
	static {
        for (ErrorCode legEnum : ErrorCode.values()) {
            map.put(legEnum.errorCode, legEnum);
        }
    }

    public static ErrorCode valueOf(int errorCode) {
        return map.containsKey(errorCode) ? map.get(errorCode) : ErrorCode.UNKNOW;
    }
	
}
