package br.com.spotdev.olxmessager.scrapper.enums;

/**
 * @Descri��o Enum de estados do OLX
 * 
 * @Autor Davi Salles
 * @Data 14/04/2016
 */

public enum Estado {

	BRASIL,
	AC,	 
	AL,	 
	AP,	 
	AM,	 
	BA,	 
	CE,	 
	DF,	 
	ES,	 
	GO,	 
	MA,	 
	MT,	 
	MS,	 
	MG,	 
	PA,	 
	PB,	 
	PR,	 
	PE,	 
	PI,	 
	RJ,	 
	RN,	 
	RS,	 
	RO,	 
	RR,	 
	SC,	 
	SP,	 
	SE,	 
	TO;
	
}
