package br.com.spotdev.olxmessager.scrapper.enums;

/**
 * @Descri��o Enums de tipos de anuncios do OLX
 * 
 * @Autor Davi Salles
 * @Data 14/04/2016
 */

public enum Tipo {

	TODOS(""),
	PROFISSIONAL("f=c"),
	PARTICULAR("f=p");
	
	private String value;
	
	Tipo(String value){
		this.value = value;
	}
	
	public String getValue(){
		return value;
	}
	
}
