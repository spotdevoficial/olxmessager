package br.com.spotdev.olxmessager.scrapper.events;

import br.com.spotdev.olxmessager.scrapper.connection.Connection;
import br.com.spotdev.olxmessager.scrapper.connection.Envio;

/**
 * @Descri��o Evento de quando uma mensagem � enviada no olx
 * 
 * @Autor Davi Salles
 * @Data 14/04/2016
 */

public interface OLXEnvioEvent {

	/**
	 * @description Fun��o chamada quando o envio � realizado
	 * @param envio Objeto que representa a mensagem enviada 
	 * e seus status
	 */
	public void envioRealizado(Envio envio);
	
	/**
	 * @description Função chamada quando o envio é iniciado
	 */
	public void envioIniciado(Connection connection);
	
	/**
	 * @description Função chamada quando o envio é finalizado
	 * @param connection Conexão do evento
	 */
	public void envioFinalizado(Connection connection);
	
}
