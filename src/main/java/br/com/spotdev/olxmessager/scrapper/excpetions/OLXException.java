package br.com.spotdev.olxmessager.scrapper.excpetions;

import br.com.spotdev.olxmessager.scrapper.enums.ErrorCode;

public class OLXException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5399435855895548904L;
	String message = null;
	ErrorCode code = ErrorCode.UNKNOW;
	
	public OLXException(String message, ErrorCode code){
		this.code = code;
		this.message = message;
		setMessage(message);
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public ErrorCode getCode() {
		return code;
	}

	public void setCode(ErrorCode code) {
		this.code = code;
	}
	
}
