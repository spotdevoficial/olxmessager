package br.com.spotdev.olxmessager.scrapper.excpetions;

/**
 * @Descricao Excpetion de caso o envio já tenha sido realizado para
 * um sellerid específico
 * 
 * @Autor Davi Salles
 * @Data 30/04/2016
 */


public class SellerAlreadyComunicated extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7890299860329881408L;
	
	public SellerAlreadyComunicated(){
		super("Este vendedor já foi comunicado");
	}

}
