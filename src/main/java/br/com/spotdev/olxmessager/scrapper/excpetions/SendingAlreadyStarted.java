package br.com.spotdev.olxmessager.scrapper.excpetions;

/**
 * @Descri��o Exception de caso j� tenha iniciado o envio e se tente novamente
 * 
 * @Autor Davi Salles
 * @Data 14/04/2016
 */

public class SendingAlreadyStarted extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6959792684138260856L;
	
	public SendingAlreadyStarted(){
		super("J� existe um envio iniciado");
	}

}
