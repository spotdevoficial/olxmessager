package br.com.spotdev.olxmessager.scrapper.webwhatsapp;

import java.util.ArrayList;
import java.util.List;

abstract class AbstractWhatsAPPSearch {

	/**
	 * Lista de variáveis privadas
	 */
	private List<WhatsAPPFound> whatsFounds = new ArrayList<>();	// URLs encontradas no texto
	private String text = null;									// Texto em que a busca foi realizada
	
	/**
	 * Construtor da classe
	 * @param text string em que será buscado os domínios
	 */
	public AbstractWhatsAPPSearch(String text){
		this.text = text;
	}
	
	/**
	 * GETTERS AND SETTERS
	 */
	
	
	public String getText() {
		return text;
	}

	public List<WhatsAPPFound> getWhatsFounds() {
		return whatsFounds;
	}
	
	
}
