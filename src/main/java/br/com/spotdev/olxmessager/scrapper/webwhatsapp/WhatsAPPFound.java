package br.com.spotdev.olxmessager.scrapper.webwhatsapp;

import br.com.spotdev.olxmessager.scrapper.Utils;

/**
 * @Descrição Classe que armazena um domínio encontrado
 * 
 * @Autor Davi Salles
 * @Data 24/01/2016
 */
public class WhatsAPPFound {

	/**
	 * Variáveis privadas da classe
	 */
	private int fromIndex = -1;		// Index em que a URL inicia na mensagem
	private int toIndex = -1;		// Index em que a URL termina na mensagem
	private String whats = null;
	
	
	/**
	 * GETTERS AND SETTERS
	 */
	
	public int getFromIndex() {
		return fromIndex;
	}
	public void setFromIndex(int fromIndex) {
		this.fromIndex = fromIndex;
	}
	public int getToIndex() {
		return toIndex;
	}
	public void setToIndex(int toIndex) {
		this.toIndex = toIndex;
	}
	public String getWhats() {
	    String toReurn = whats.replaceAll("( )|(\\()|(\\))|(-)", "");
		return toReurn+" "+Utils.getHebraico(3, true);
	}
    public void setWhats(String whats) {
        this.whats = whats;
    }
    public void dispose(){
        this.whats = null;
    }
    @Override
	public String toString(){
		return getWhats().toString();
	}
	
}
