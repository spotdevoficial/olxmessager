package br.com.spotdev.olxmessager.scrapper.webwhatsapp;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @Descrição Classe que busca por domínios em textos
 * 
 * @Autor Davi Salles
 * @Data 24/01/2016
 */
public class WhatsAPPSearch extends AbstractWhatsAPPSearch {

    Pattern PATTERN = Pattern.compile("\\+?( )?([0-9]{2})?( )?([(])?([1-9]{2})"
            + "(\\))?( )?9?[0-9]{4}( )?-?( )?[0-9]{4}\\b");
    
	public WhatsAPPSearch(String text) {
		super(text);
		search();
	}

	/**
	 * Função que inicia a busca 
	 * 
	 * @return true caso tenha encontrado algo
	 */
	private boolean search(){
		clearWhatss();
		String stringToSearch = getText().toLowerCase();
		
		Matcher match = PATTERN.matcher(stringToSearch);
		// Inicia a busca por todas as extensões de domínios
		while(match.find()){
		    int start = match.start();
		    int end = match.end();
			WhatsAPPFound found = new WhatsAPPFound();
			found.setFromIndex(start);
			found.setToIndex(end);
			found.setWhats(stringToSearch.substring(start, end));
			
			this.getWhatsFounds().add(found);
		}
		
		return hasWhats();
	}
	
	/**
	 * Retorna se a última busca tinha domínio
	 * 
	 * @return falso caso não tenha domínios
	 */
	public boolean hasWhats(){
		return getWhatsFounds().size() <= 0;
	}
	
	/**
	 * Limpa a lista de domínios e da dispose nos objetos de dentro
	 */
	private void clearWhatss(){
		for(WhatsAPPFound whatsFound : getWhatsFounds()){
			whatsFound.dispose();
		}
		
		getWhatsFounds().clear();
	}
	
	/**
	 * Retorna o texto encurtado
	 * 
	 * @return o mesmo que o getText mas encurtado
	 */
	
	public String getShorteredText(){
		StringBuilder builder = new StringBuilder(getText());
		int compensador = 0;
		
		for(WhatsAPPFound found : getWhatsFounds()){
			
			if(found.getWhats() == null)
				continue;
			
			int sizeUrl = found.getToIndex()-found.getFromIndex();
			int sizeShortered = found.getWhats().toString().length();
			builder.delete(found.getFromIndex()+compensador, found.getToIndex()+compensador);
			builder.insert(found.getFromIndex()+compensador, found.getWhats());
			compensador+=sizeShortered-sizeUrl;
		}
		
		return builder.toString();
		
	}
	
	
}
